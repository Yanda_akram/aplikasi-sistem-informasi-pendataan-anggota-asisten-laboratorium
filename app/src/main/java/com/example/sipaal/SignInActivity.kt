package com.example.sipaal

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(), View.OnClickListener {

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        imLogout.setOnClickListener {
            fbAuth.signOut()
        }
        imLogout.setOnClickListener(this)
        imUpload.setOnClickListener(this)
        imAnggota.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imLogout -> {
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                Toast.makeText(this,"Logoff berhasil dilakukan", Toast.LENGTH_SHORT).show()
            }
            R.id.txLogout -> {
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                Toast.makeText(this,"Logoff berhasil dilakukan", Toast.LENGTH_SHORT).show()
            }
            R.id.imUpload -> {
                val intent = Intent(this,Upload::class.java)
                startActivity(intent)
                Toast.makeText(this,"Upload Berkas", Toast.LENGTH_SHORT).show()
            }
            R.id.imAnggota -> {
                val intent = Intent(this,anggota::class.java)
                startActivity(intent)
                Toast.makeText(this,"Biodata Anggota", Toast.LENGTH_SHORT).show()
            }
        }
    }
}